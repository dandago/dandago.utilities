﻿using Dandago.Utilities.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.Examples.ScopedAsyncLockParallel
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ScopedAsyncLock in Parallel";

            RunAll();
            Console.ReadLine();

            Console.WriteLine("Disposing...");
            factory.Dispose();
            factory.Dispose(); // just making sure that calling this twice doesn't break it
            Console.WriteLine("Disposed. Press any key to exit.");

            Console.ReadKey();
        }

        private static ScopedAsyncLockFactory factory = new ScopedAsyncLockFactory();

        static void RunAll()
        {
            Parallel.For(1, 1000, i => RunJob("Job " + i));
        }

        static async void RunJob(string str)
        {
            using (var scopedLock = await factory.CreateLockAsync())
            {
                Console.WriteLine("Start " + str);

                await Task.Delay(700);

                Console.WriteLine("End " + str);
            }
        }
    }
}
