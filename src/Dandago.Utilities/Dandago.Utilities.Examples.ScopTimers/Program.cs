﻿using Dandago.Utilities.ScopedTimers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dandago.Utilities.Examples.ScopTimers
{
    class Program
    {
        static void Main(string[] args)
        {
            // Example 1: Basic

            {
                var scopedTimerFactory = new ScopedTimerFactory<ConsoleScopedTimer, StopwatchTimeKeeper>();

                using (var timer = scopedTimerFactory.Create("Benchmark"))
                {
                    Console.WriteLine("Doing stuff!");
                    Thread.Sleep(100);
                }

                using (var timer = scopedTimerFactory.Create("Example 1 Run 2"))
                {
                    Console.WriteLine("Doing more stuff!");
                    Thread.Sleep(100);
                }
            }

            // Example 2: Dependency Injection with Ninject

            using (IKernel kernel = new StandardKernel())
            {
                kernel.Bind<IScopedTimerFactory>()
                      .To<ScopedTimerFactory<ConsoleScopedTimer, StopwatchTimeKeeper>>();

                var scopedTimerFactory = kernel.Get<IScopedTimerFactory>();

                using (var timer = scopedTimerFactory.Create("Example 2 Run 1"))
                {
                    Console.WriteLine("Doing stuff with DI!");
                    Thread.Sleep(100);
                }

                using (var timer = scopedTimerFactory.Create("Example 2 Run 2"))
                {
                    Console.WriteLine("Doing more stuff with DI!");
                    Thread.Sleep(100);
                }
            }

            Console.ReadLine();
        }
    }
}
