﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dandago.Utilities.Threading
{
    public class ScopedAsyncLock : IDisposable
    {
        private SemaphoreSlim sem;

        public ScopedAsyncLock(SemaphoreSlim sem)
        {
            this.sem = sem;
        }

        public async Task LockAsync()
        {
            await this.sem.WaitAsync(); // wait until lock is free
        }

        public void Dispose()
        {
            sem.Release(); // free the lock
        }
    }
}
