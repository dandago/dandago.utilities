﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dandago.Utilities.Threading
{
    public class ScopedAsyncLockFactory : IDisposable
    {
        private bool disposed = false;
        private SemaphoreSlim sem;

        public ScopedAsyncLockFactory()
        {
            this.sem = new SemaphoreSlim(1, 1);
        }

        public async Task<ScopedAsyncLock> CreateLockAsync()
        {
            var scopedLock = new ScopedAsyncLock(this.sem);
            await scopedLock.LockAsync();
            return scopedLock;
        }

        #region IDisposable stuff

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && this.sem != null)
            {
                this.sem.Dispose();
                this.sem = null;
            }

            disposed = true;
        }


        #endregion IDisposable stuff
    }
}
