﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public interface ITimeKeeper
    {
        void Start();
        TimeSpan Stop();
    }
}
