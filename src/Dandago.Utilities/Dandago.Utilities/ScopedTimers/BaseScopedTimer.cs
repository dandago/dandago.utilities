﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public abstract class BaseScopedTimer : IScopedTimer
    {
        private string name;
        private ITimeKeeper timeKeeper;

        protected string Name => this.name;

        public void Init(string name, ITimeKeeper timeKeeper)
        {
            this.name = name;
            this.timeKeeper = timeKeeper;

            this.WriteBegin();
            this.timeKeeper.Start();
        }

        protected abstract void WriteBegin();

        protected abstract void WriteEnd(TimeSpan elapsedTime);

        public virtual void Dispose()
        {
            var elapsed = this.timeKeeper.Stop();
            this.WriteEnd(elapsed);
        }
    }
}
