﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public class ConsoleScopedTimer : BaseScopedTimer
    {
        protected override void WriteBegin()
        {
            Console.WriteLine($">> Begin {this.Name}");
        }

        protected override void WriteEnd(TimeSpan elapsedTime)
        {
            Console.WriteLine($"<< End {this.Name} - took {elapsedTime}");
        }
    }
}
