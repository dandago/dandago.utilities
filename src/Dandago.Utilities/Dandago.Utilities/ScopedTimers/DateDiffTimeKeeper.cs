﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public class DateDiffTimeKeeper : ITimeKeeper
    {
        private DateTime startTime;

        public DateDiffTimeKeeper()
        {
            
        }

        public void Start()
        {
            this.startTime = DateTime.UtcNow;
        }

        public TimeSpan Stop()
        {
            var endTime = DateTime.UtcNow;
            var elapsed = endTime - this.startTime;
            return elapsed;
        }
    }
}
