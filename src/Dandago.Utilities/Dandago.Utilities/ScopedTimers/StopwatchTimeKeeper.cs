﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public class StopwatchTimeKeeper : ITimeKeeper
    {
        private Stopwatch stopwatch;

        public StopwatchTimeKeeper()
        {
            this.stopwatch = new Stopwatch();
        }

        public void Start()
        {
            this.stopwatch.Start();
        }

        public TimeSpan Stop()
        {
            this.stopwatch.Stop();
            return this.stopwatch.Elapsed;
        }
    }
}
