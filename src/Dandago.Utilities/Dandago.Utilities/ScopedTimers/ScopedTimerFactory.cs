﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public class ScopedTimerFactory<TTimer, TKeeper>
        : IScopedTimerFactory where TTimer : IScopedTimer, new()
                              where TKeeper : ITimeKeeper, new()
    {
        public ScopedTimerFactory()
        {
            
        }

        public IScopedTimer Create(string name)
        {
            var timeKeeper = new TKeeper();
            var scopedTimer = new TTimer();
            scopedTimer.Init(name, timeKeeper);
            return scopedTimer;
        }
    }
}
