﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.ScopedTimers
{
    public interface IScopedTimer : IDisposable
    {
        void Init(string name, ITimeKeeper timeKeeper);
    }
}
