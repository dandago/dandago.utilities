﻿using Dandago.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.Examples.IEnumerableExt
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list1 = null;
            List<int> list2 = new List<int>();
            List<int> list3 = new List<int>() { 1 };

            Console.WriteLine("list1 null/empty: " + list1.IsNullOrEmpty());
            Console.WriteLine("list2 null/empty: " + list2.IsNullOrEmpty());
            Console.WriteLine("list3 null/empty: " + list3.IsNullOrEmpty());

            Console.ReadLine();
        }
    }
}
