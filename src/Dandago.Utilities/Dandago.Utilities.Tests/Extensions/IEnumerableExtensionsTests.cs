﻿using Dandago.Utilities.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.Tests.Extensions
{
    public class IEnumerableExtensionsTests
    {
        [TestCase(null, true)]
        [TestCase(new int[0], true)]
        [TestCase(new int[1] { 1 }, false)]
        public void IsNullOrEmptyTests(IEnumerable<int> enumerable, bool expected)
        {
            // act

            bool actual = enumerable.IsNullOrEmpty();

            // assert

            Assert.AreEqual(expected, actual);
        }
    }
}
