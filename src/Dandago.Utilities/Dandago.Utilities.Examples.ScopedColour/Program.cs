﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Utilities.Examples.ScopedColour
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Normal Text!");

            using (var colour = new ScopedConsoleColour(ConsoleColor.Red))
                Console.WriteLine("Red Text!");

            Console.WriteLine("Normal Text Again!");

            Console.ReadLine();
        }
    }
}
